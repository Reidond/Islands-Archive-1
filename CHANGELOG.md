﻿# Список изменений
Все заметные изменения в этом проекте будут задокументированы в этом файле.

Формат основан на [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
и этот проект придерживается [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Текущий список модификаций:

1. [CUP Terrains - Core](https://steamcommunity.com/workshop/filedetails/?id=583496184)
2. [CUP Terrains - Maps](https://steamcommunity.com/sharedfiles/filedetails/?id=583544987)
3. [RHSAFRF](https://steamcommunity.com/sharedfiles/filedetails/?id=843425103)
4. [RHSUSAF](https://steamcommunity.com/sharedfiles/filedetails/?id=843577117)
5. [RHSGREF](https://steamcommunity.com/sharedfiles/filedetails/?id=843593391)
6. [RHSSAF](https://steamcommunity.com/sharedfiles/filedetails/?id=843632231)
7. [YuEmod](https://steamcommunity.com/sharedfiles/filedetails/?id=1195262135)
8. [Spec Ops](https://gitlab.com/Reidond/SpecOps)
9. [Islands](https://gitlab.com/Reidond/Islands)

## [1.7.1] - 2019-05-04

### Добавлено

* [Kujari](https://steamcommunity.com/sharedfiles/filedetails/?id=1726494027)

### Обновлено

* [Namalsk Light Patch V2](https://steamcommunity.com/sharedfiles/filedetails/?id=1618365602)
* [Namalsk CUP Replace](https://steamcommunity.com/sharedfiles/filedetails/?id=1618441957)

### Удалено

* [Thirsk](http://www.armaholic.com/page.php?id=9861) Невостребован
* [Pulau](https://steamcommunity.com/sharedfiles/filedetails/?id=1423583812) Невостребован

## [1.7] - 2019-02-27

### Добавлено

+ [Namalsk](https://steamcommunity.com/sharedfiles/filedetails/?id=1125856428)
+ [Namalsk Light Patch V2](https://steamcommunity.com/sharedfiles/filedetails/?id=1618365602)
+ [Namalsk CUP Replace](https://steamcommunity.com/sharedfiles/filedetails/?id=1618441957) 

### Обновлено

* [Summa](https://steamcommunity.com/sharedfiles/filedetails/?id=1598087521)

## [1.6] - 2018-12-23

### Добавлено

+ [Thirsk Island](http://www.armaholic.com/page.php?id=9861)
+ [Summa](https://steamcommunity.com/sharedfiles/filedetails/?id=1598087521)

### Обновлено

* [Ihantala Winter](https://steamcommunity.com/sharedfiles/filedetails/?id=1494127420)

## [1.5] - 2018-11-24

### Добавлено

+ [Pulau](https://steamcommunity.com/sharedfiles/filedetails/?id=1423583812)
+ [Anizay](https://steamcommunity.com/sharedfiles/filedetails/?id=1537973181)
+ [Ihantala Winter](https://steamcommunity.com/sharedfiles/filedetails/?id=1494127420)

## [1.4] - 2018-09-23

### Добавлено

+ [Chernarus 2035](http://www.armaholic.com/page.php?id=33127)
+ [CUP ACE3 Compatibility Addon - Terrains](https://steamcommunity.com/sharedfiles/filedetails/?id=1375890861&searchtext=ace+3+terrains)

### Удалено

- F.A.T.A. - невостребованна
- Valtatie 5 (VT5) - невостребованна
- ns_modules - невостребован
- Namalsk_A3 (заменен на опциональный из S.T.A.L.K.E.R.- Live Zone)
